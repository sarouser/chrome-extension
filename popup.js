chrome.runtime.onMessage.addListener(gotMessage);

function gotMessage(message, sender, sendresponse) {
    let reg = "^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)?\\atlas.immobilienscout24.de\\/adresse\\/.+";
    let txt;
    let paragraphs = document.getElementsByTagName("a");
    let listOfHrefs = [];
    let listOfParents = [];
    let listOfTags = [];
    let j = 0;
    for (let i = 0; i < paragraphs.length; i++) {
        txt = paragraphs[i].href;
        if (RegExp(reg).test(txt)) {
            listOfHrefs[j] = txt;
            listOfTags[j] = paragraphs[i];
            listOfParents[j] = paragraphs[i].parentElement;
            j++;
        }
    }
    for (let i = 0; i < listOfHrefs.length; i++) {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState === XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
                if (xmlhttp.status === 200) {
                    let html = xmlhttp.responseText;
                    let pHs = html.indexOf('{"privateHouseholds');
                    let dR = html.indexOf("families");
                    //console.log(html.substring(pHs, (dR-2)));
                    let together = html.substring(pHs, (dR - 2));
                    together += '}';
                    let json = JSON.parse(together);
                    let householdNum = json['privateHouseholds'];
                    let resAge = json['residentsAge'];
                    let durOfRes = json['durationOfResidence'];
                    //case highest
                    if ((resAge === '65+' || resAge === '>65') && (durOfRes === '+10' || durOfRes === '>10')) {
                        listOfTags[i].innerText += ('(Highest,Households:' + householdNum + ')');
                        listOfParents[i].style['background-color'] = '#006400';
                    }
                    //end of case highest
                    //case high
                    if ((resAge === '65+' || resAge === '60-64') && (durOfRes === '+10' || durOfRes === '>10' || durOfRes === '8-10' || durOfRes === '6-8' || durOfRes === 'k. A.')) {
                        listOfTags[i].innerText += ('(High,Households:' + householdNum + ')');
                        listOfParents[i].style['background-color'] = '#90EE90';
                    }
                    //end of case high

                    //case none
                    if (resAge === '18-29' || resAge === '30-34' || resAge === '35-39') {
                        listOfTags[i].innerText += ('(None,Households:' + householdNum + ')');
                    }
                    //end of case none

                    //case normal
                    if (resAge === '50-54' || resAge === '55-59' || resAge === '65+' || resAge === '60-64') {
                        listOfTags[i].innerText += ('(Normal,Households:' + householdNum + ')');
                        listOfParents[i].style['background-color'] = '#FFFF00';
                    }
                    //end of case normal

                    //case low
                    if (resAge === '40-44' || resAge === '45-49') {
                        listOfTags[i].innerText += ('(Low,Households:' + householdNum + ')');
                        listOfParents[i].style['background-color'] = '#FFA500';
                    }
                    //end of case low

                } else if (xmlhttp.status === 400) {
                    console.log('There was an error 400');
                } else {
                    console.log('Something else other than 200 was returned');
                }
            }
        };
        xmlhttp.open("GET", listOfHrefs[i], false);
        xmlhttp.send();
    }
}